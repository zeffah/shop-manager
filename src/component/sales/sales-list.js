import React, { Component } from 'react'
import { Accordion, Icon, Segment, List, Table, Pagination, Container, Message } from 'semantic-ui-react'
export class SalesListComponent extends Component {
    constructor(props){
        super(props)
        this.state = { activeIndex: 0 }
    }

  handleClick = (e, titleProps) => {
    const { index } = titleProps
    const { activeIndex } = this.state
    const newIndex = activeIndex === index ? -1 : index

    this.setState({ activeIndex: newIndex })
  }

  render() {
    const { activeIndex } = this.state
    const {sales} = this.props
    return (
        <ShowContent sales={sales} activeIndex={activeIndex} handleClick={this.handleClick}/>
    )
  }
}

const ShowContent = ({sales, activeIndex, handleClick}) => (
    sales.length > 0 ? <AccordionList sales={sales} activeIndex={activeIndex} handleClick={handleClick} /> : <MessageNegative>No sales found</MessageNegative>
)

const MessageNegative = () => (
    <Message negative>
        <Message.Header>No sales Records</Message.Header>
        <p>Sorry, seems you don't have any sales to display</p>
    </Message>
)

const AccordionList = ({sales, activeIndex, handleClick}) => (
    <div>
        <Segment inverted>
            <Accordion inverted>
                {
                    sales.map(sale => (
                        <div key={sale.id}>
                            <Accordion.Title active={activeIndex === sale.id} index={sale.id} onClick={handleClick}>
                                <Icon name='dropdown' />
                                <ListSaleItem sale={sale} />
                            </Accordion.Title>
                            {/* <Divider /> */}
                            <Accordion.Content active={activeIndex === sale.id}>
                                <ListSaleDetails saleDetails={sale.sale_items} />
                            </Accordion.Content>
                        </div>
                    ))
                }
            </Accordion>
        </Segment>
        <PaginationExampleShorthand />
    </div>
)

const ListSaleItem = ({sale}) => (
    <List horizontal>
        <List.Item>
            {/* <Image avatar src='https://react.semantic-ui.com/images/avatar/small/daniel.jpg' /> */}
            <List.Content>
                {'SHP001-SL001'}
            </List.Content>
        </List.Item>
        <List.Item>
            <List.Content>
                {sale.staff}
            </List.Content>
        </List.Item>
        <List.Item>
            <List.Content>
                {sale.shop_name}
            </List.Content>
        </List.Item>
        <List.Item>
            <List.Content>
                {`Tsh. ${sale.sale_amount}`}
            </List.Content>
        </List.Item>
        <List.Item>
            <List.Content>
                {sale.created_at}
            </List.Content>
        </List.Item>
    </List>
)

const ListSaleDetails = ({saleDetails}) => (
    <Table striped inverted>
        <Table.Header>
            <Table.Row>
                <Table.HeaderCell>ProductName</Table.HeaderCell>
                <Table.HeaderCell>Selling Price</Table.HeaderCell>
                <Table.HeaderCell>Quantity</Table.HeaderCell>
                <Table.HeaderCell>Amount</Table.HeaderCell>
            </Table.Row>
            {
                saleDetails.map(saleDetail => (
                    <Table.Row key={saleDetail.id}>
                        <Table.Cell>{saleDetail.product_name}</Table.Cell>
                        <Table.Cell>{saleDetail.selling_price}</Table.Cell>
                        <Table.Cell>{saleDetail.quantity}</Table.Cell>
                        <Table.Cell>{saleDetail.item_amount}</Table.Cell>
                    </Table.Row>
                ))
            }
        </Table.Header>
    </Table>
)

const PaginationExampleShorthand = () => (
    <Container textAlign='center'>
        <Pagination
            defaultActivePage={5}
            ellipsisItem={{ content: <Icon name='ellipsis horizontal' />, icon: true }}
            firstItem={{ content: <Icon name='angle double left' />, icon: true }}
            lastItem={{ content: <Icon name='angle double right' />, icon: true }}
            prevItem={{ content: <Icon name='angle left' />, icon: true }}
            nextItem={{ content: <Icon name='angle right' />, icon: true }}
            totalPages={10}/>
    </Container>
)