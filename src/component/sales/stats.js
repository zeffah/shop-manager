import React, { Component } from 'react'
import DatePicker from 'react-datepicker'
import moment from 'moment'
import NumberFormat from 'react-number-format'
import { Container, Grid, Statistic, Form, Table, Divider, Button } from 'semantic-ui-react'
import { Pie } from 'react-chartjs-2'
import { TotalSalesAmount, ShopsSales, InventoryList, TotalInventoryValue, InventoryPieChartDataset, SalesPieChartDataset } from '../../container/utils'

export class SaleStatistics extends Component {
    constructor(props) {
        super(props)
        this.state = {
            shopsList: [],
            totalSaleAmount: 0,
            allSales: []
        }
    }

    onFormSubmit = (e) => {
        e.preventDefault()
        let allSales = ShopsSales(this.state.shopsList)
        if (this.state.startDate === undefined && this.state.endDate === undefined) {
            console.log('Please select either start date or end date')
            return
        }
        let salesForSelectedPeriod = allSales.filter(sale => this.state.startDate.startOf('day') < moment(sale.created_at, 'YYYY-MM-DD HH:mm:ss') && this.state.endDate.endOf('day') > moment(sale.created_at, 'YYYY-MM-DD HH:mm:ss'))
        this.setState({
            totalSaleAmount: TotalSalesAmount(salesForSelectedPeriod),
            allSales: salesForSelectedPeriod
        })
    }

    onShopSelected = (value) => {
        this.setState({
            shop_id: value,
        })
    }

    handleChangeStart(date) {
        this.setState({
            startDate: moment(date)
        });
    }

    handleChangeEnd(date) {
        this.setState({
            endDate: moment(date)
        });
    }

    handleSelectStart(value) {
        this.setState({
            startDate: moment(value)
        })
    }

    handleSelectEnd(value) {
        this.setState({
            endDate: moment(value)
        })
    }

    componentDidMount() {
        fetch(`/api/shopsAPI/shops`).then(res => res.json()).then(response => {
            this.setState({
                shopsList: response.shops
            })
        })
    }

    inventoryPieChartData = (shopsList) => {
        return {
            labels: shopsList.map(shop => (shop.shop_name)),
            datasets: [{
                backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C"],
                borderColor: ["#F7464A", "#46BFBD", "#FDB45C"],
                data: InventoryPieChartDataset(shopsList),
            }]
        }
    }

    salesPieChartData = (shopsList) => {
        return {
            labels: shopsList.map(shop => (shop.shop_name)),
            datasets: [{
                backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C"],
                borderColor: ["#A8464A", "#A8BFBD", "#A8B45C"],
                data: SalesPieChartDataset(shopsList),
            }]
        }
    }

    render() {
        const selectoptions = this.state.shopsList.map(shop => (
            { key: shop.id, value: shop.id, text: shop.shop_name }
        ))
        let inventoryData = this.inventoryPieChartData(this.state.shopsList)
        let salesData = this.salesPieChartData(this.state.shopsList)
        const _options = { responsive: true }

        let shopInventory = this.state.shopsList.map(shop => (
            { id: shop.id, shop_name: shop.shop_name, total_amount: TotalInventoryValue(shop.inventory) }
        ))

        let shopsInventories = InventoryList(this.state.shopsList)
        let totalInventory = TotalInventoryValue(shopsInventories)

        let allSales = ShopsSales(this.state.shopsList)
        console.log(allSales)
        let totalSaleAmount = TotalSalesAmount(allSales)
        // let salesForSelectedPeriod = allSales.filter(sale => moment().startOf(this.state.startDate) < moment(sale.created_at, 'YYYY-MM-DD HH:mm:ss') && moment().endOf(this.state.endDate) > moment(sale.created_at, 'YYYY-MM-DD HH:mm:ss'))
        console.log(SalesPieChartDataset(this.state.shopsList))

        return (
            <Grid>
                <Grid.Column textAlign='center' width='8'>
                    {'Tsh.'}
                    <Statistic size='small' color='green'>
                        <Statistic.Value><NumberFormat value={totalInventory} displayType={'text'} thousandSeparator={true} prefix={''} /></Statistic.Value>
                        <Statistic.Label>Inventory</Statistic.Label>
                    </Statistic>
                    <div>
                        <Table striped>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell>Shop Name</Table.HeaderCell>
                                    <Table.HeaderCell>Total Amount</Table.HeaderCell>
                                </Table.Row>
                                {
                                    shopInventory.map(shopInv => (
                                        <Table.Row key={shopInv.id}>
                                            <Table.Cell>{shopInv.shop_name}</Table.Cell>
                                            <Table.Cell>{`${shopInv.total_amount}`}</Table.Cell>
                                        </Table.Row>
                                    ))
                                }
                            </Table.Header>
                        </Table>
                    </div>
                    <div>
                        <Container>
                            <Pie data={inventoryData} options={_options} />
                        </Container>
                    </div>
                </Grid.Column>
                <Grid.Column width='8'>
                    <Statistic.Group size='small' widths='two'>
                        <Statistic size='small' color='green'>
                            <Statistic.Value><NumberFormat value={totalSaleAmount} displayType={'text'} thousandSeparator={true} prefix={''} /></Statistic.Value>
                            <Statistic.Label>Sales</Statistic.Label>
                        </Statistic>
                        <Statistic size='small' color='green'>
                            <Statistic.Value><NumberFormat value={this.state.totalSaleAmount} displayType={'text'} thousandSeparator={true} prefix={''} /></Statistic.Value>
                            <Statistic.Label>Daily Sales</Statistic.Label>
                        </Statistic>
                    </Statistic.Group>
                    <Divider />
                    <Container>
                        <Form onSubmit={(e) => this.onFormSubmit(e)}>
                            <Form.Select placeholder='Select shop' options={selectoptions} onChange={(e, { value }) => this.onShopSelected(value)}></Form.Select>
                            <Grid>
                                <Grid.Column width='7'>
                                    <DatePicker
                                        placeholderText={`Start Date`}
                                        todayButton={"Today"}
                                        dateFormat="DD/MM/YYYY"
                                        selected={this.state.startDate}
                                        onSelect={(value) => this.handleSelectStart(value)}
                                        onChange={(value) => this.handleChangeStart(value)} />
                                </Grid.Column>
                                <Grid.Column width='7'>
                                    <DatePicker
                                        placeholderText="End Date"
                                        todayButton={"Today"}
                                        dateFormat="DD/MM/YYYY"
                                        selected={this.state.endDate}
                                        onSelect={(value) => this.handleSelectEnd(value)}
                                        onChange={(value) => this.handleChangeEnd(value)} />
                                </Grid.Column>
                                <Grid.Column width='2'>
                                    <Button type='submit' content='Find' secondary />
                                </Grid.Column>
                            </Grid>
                        </Form>
                        <Pie data={salesData} options={_options} />
                    </Container>
                </Grid.Column>
                {/* <Grid.Column width='16'>
                    Hi here
                </Grid.Column> */}
            </Grid>
        )
    }
}