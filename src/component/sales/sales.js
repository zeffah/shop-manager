import React from 'react'
import {Grid, Form, Divider, Button} from 'semantic-ui-react'
import DatePicker from 'react-datepicker';
import moment from 'moment';
import {SalesListComponent} from './sales-list'

export class SaleComponent extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            startDate: null,
            endDate: null,
            shopList: [],
            salesList: [],
            shop_id: ""
        }
    }

    onShopSelected = (value) => {
        this.setState({
            shop_id: value
        })
    }

    handleChangeStart(date) {
        this.setState({
            startDate: moment(date)
        });
    }

    handleChangeEnd(date) {
        this.setState({
            endDate: moment(date)
        });
    }

    handleSelectStart(value){
        this.setState({
            startDate: moment(value)
        })
    }

    handleSelectEnd(value) {
        this.setState((prevState)=>({
            endDate: moment(value)
        }))
    }

    generateSalesList(_shopsList){
        let mSales = []
        _shopsList.map(shop => {
            mSales = [...mSales, ...JSON.parse(shop.sales)]
            return mSales
         })
        return mSales
    }
    
    componentDidMount(){
        fetch(`/api/salesAPI/sales`)
            .then(response => response.json())
            .then(res => {
                this.setState({
                    shopList: res.shops,
                    salesList: this.generateSalesList(res.shops),
                    shop_id: ""
                })
            })
            .catch(error => console.log(error))
    }

    handleSubmit = () => {
        let { startDate, endDate, shop_id } = this.state
        if (startDate === null || endDate === null || shop_id === "") {
            return
        }
        if(startDate.isAfter(endDate)){
            console.log(`Start date can't be after end date`);
            return
        }
        // let data = { start_date: startDate.unix(), end_date: endDate.unix(), shop_id: shop_id }
        let shop = this.state.shopList.filter(shop => shop.id === this.state.shop_id)
        this.setState({
            salesList: this.generateSalesList(shop)
        })
    }

    render(){
        let options = this.state.shopList.map((shop, i) => (
            { key: i, text: shop.shop_name, value: shop.id }
        ))
        return (
            <div>
                <Grid>
                    <Grid.Column width={2}></Grid.Column>
                    <Grid.Column width={12}>
                        <Form onSubmit={(e)=>this.handleSubmit(e)}>
                            <Grid>
                                <Grid.Column width={6}>
                                    <Form.Select onChange={(e, { value }) => this.onShopSelected(value)} placeholder="Select Shop" options={options} />
                                </Grid.Column>
                                <Grid.Column width={4}>
                                    <DatePicker
                                        placeholderText="Select start date"
                                        todayButton={"Today"}
                                        dateFormat="DD/MM/YYYY"
                                        selected={this.state.startDate}
                                        onSelect={(value) => this.handleSelectStart(value)}
                                        onChange={(value) => this.handleChangeStart(value)} />
                                </Grid.Column>
                                <Grid.Column width={4}>
                                    <DatePicker
                                        placeholderText="Select end date"
                                        todayButton={"Today"}
                                        placeholder={`End Date`}
                                        dateFormat="DD/MM/YYYY"
                                        selected={this.state.endDate}
                                        onSelect={(value) => this.handleSelectEnd(value)}
                                        onChange={(value) => this.handleChangeEnd(value)} />
                                </Grid.Column>
                                <Grid.Column width={2}>
                                    <Button content='Search' inverted color="green" type="submit" />
                                </Grid.Column>
                            </Grid>
                        </Form>
                    </Grid.Column>
                    <Grid.Column width={2}></Grid.Column>
                </Grid>
                <Divider />
                <SalesListComponent sales={this.state.salesList}/>
            </div>
        )
    }
}