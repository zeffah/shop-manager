import React, {Component} from 'react'
import { Button, Icon, Modal, Form } from 'semantic-ui-react'

export class ModalEditProduct extends Component {
    constructor(props) {
        super(props)
        this.state = {
            product_name: this.props.product.product_name,
            product_type: this.props.product.product_type,
            product_supplier: this.props.product.product_supplier,
            product_category: this.props.product.product_category,
            buying_price: this.props.product.buying_price,
            product_manufacturer: this.props.product.product_manufacturer,
            id: this.props.product.product_id
        }
    }

    handleClose = (e) => {
        e.preventDefault()
        this.props.editModalOpen(false)
    }

    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    handleSubmit = (e) => {
        e.preventDefault()
        fetch('/api/productsAPI/update', { method: 'POST', body: JSON.stringify(this.state) })
            .then(success => {
                return success.json()
            }).then(response => {
                let data = response.data
                this.props.product.product_name = data.product_name
                this.props.product.product_type = data.product_type
                this.props.product.product_supplier = data.product_supplier
                this.props.product.product_category = data.product_category
                this.props.product.buying_price = data.buying_price
                this.props.product.product_manufacturer = data.product_manufacturer
                this.props.editModalOpen(false)
            }).catch((err) => {
                console.log(err)
            })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            product_name: nextProps.product.product_name,
            product_type: nextProps.product.product_type,
            product_supplier: nextProps.product.product_supplier,
            product_category: nextProps.product.product_category,
            buying_price: nextProps.product.buying_price,
            product_manufacturer: nextProps.product.product_manufacturer,
            id: nextProps.product.id
        })
    }

    render() {
        let { product } = this.props
        return (
            <Modal basic size='small' open={this.props.open} onClose={() => this.props.editModalOpen(false)}>
                <Modal.Content>
                    <h3>{`Update Product - ${product.product_name}`}</h3>
                    <Form onSubmit={(e) => this.handleSubmit(e)}>
                        <Form.Field>
                            <h4>{`Product Name`}</h4>
                            <input type="text" name="product_name" value={this.state.product_name} onChange={(e) => this.onChange(e)} />
                        </Form.Field>
                        <Form.Field>
                            <h4>{`Product Type`}</h4>
                            <input type="text" name="product_type" value={this.state.product_type} onChange={(e) => this.onChange(e)} />
                        </Form.Field>
                        <Form.Field>
                            <h4>{`Product Category`}</h4>
                            <input type="text" name="product_category" value={this.state.product_category} onChange={(e) => this.onChange(e)} />
                        </Form.Field>
                        <Form.Field>
                            <h4>{`Manufacturer`}</h4>
                            <input type="text" name="product_manufacturer" value={this.state.product_manufacturer} onChange={(e) => this.onChange(e)} />
                        </Form.Field>
                        <Form.Field>
                            <h4>{`Supplier`}</h4>
                            <input type="text" name="product_supplier" value={this.state.product_supplier} onChange={(e) => this.onChange(e)} />
                        </Form.Field>
                        <Form.Field>
                            <h4>{`Buying Price`}</h4>
                            <input type="number" name="buying_price" value={this.state.buying_price} onChange={(e) => this.onChange(e)} />
                        </Form.Field>
                        <Modal.Actions>
                            <Button type="submit" color='green' inverted><Icon name='checkmark' />Save Now</Button>
                            <Button onClick={(e) => this.handleClose(e)} color='red' inverted><Icon name='remove' />Cancel</Button>
                        </Modal.Actions>
                    </Form>
                </Modal.Content>
            </Modal>
        )
    }
}