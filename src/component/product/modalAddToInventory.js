import React from 'react'
import { Button, Header, Icon, Modal, Form } from 'semantic-ui-react'

export class ModalAddToInventory extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            selling_price: "",
            quantity: "",
            shop_id: "",
            product_id: this.props.product.id,
            shopList: []
        }
    }

    handleClose = () => {
        this.setState({
            selling_price: "",
            quantity: "",
            shop_id: "",
            product: {}
        })
        this.props.addToInventoryModalOpen(false)
    }

    onChange = (e, value) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    onShopSelected = (value) => {
        this.setState({
            shop_id: value
        })
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            product_id: nextProps.product.id
        })
    }

    componentDidMount(){
        fetch(`/api/shopsAPI/shops`)
            .then(response => response.json())
            .then(res => {
                this.setState({
                    shopList: res.shops,
                    selling_price: "",
                    quantity: "",
                    shop_id: ""
                })
            })
            .catch(error => console.log(error))
    }

    handleSubmit = (e) => {
        e.preventDefault()
        let {selling_price, quantity, product_id, shop_id} = this.state
        if(selling_price ==="" || quantity ==="" || shop_id ===""){
            return
        }
        let data = { selling_price: selling_price, quantity: quantity, product_id: product_id, shop_id: shop_id }
        fetch('/inventoryAPI/create', { method: 'POST', body: JSON.stringify(data)})
            .then(success => {
                return success.json()
            }).then(response => {
                this.handleClose()
            }).catch((err) => {
                console.log(err)
            })
    }

    render() {
        let { product } = this.props
        let options = this.state.shopList.map((shop, i)=>(
            { key: i, text: shop.shop_name, value: shop.id }
        ))
        return (
            <Modal basic size='small' open={this.props.open} onClose={() => this.handleClose()}>
                <Header icon='plus' color="green" content={`Add To Inventory - ${product.product_name} - ${product.product_type}`} />
                <Modal.Content>
                    <Form onSubmit={(e) => this.handleSubmit(e)}>
                        <h3>{`Select Shop`}</h3>
                        <Form.Select onChange={(e, { value }) => this.onShopSelected(value)} placeholder="Select Shop" options={options}>
                        </Form.Select>
                        <Form.Field>
                            <h3>{`Selling Price`}</h3>
                            <input type="number" name="selling_price" value={this.state.selling_price} onChange={(e) => this.onChange(e)} />
                        </Form.Field>
                        <Form.Field>
                            <h3>{`Quantity`}</h3>
                            <input type="number" name="quantity" value={this.state.quantity} onChange={(e) => this.onChange(e)} />
                        </Form.Field>
                        <Modal.Actions>
                            <Button type="submit" color='green' inverted><Icon name='checkmark' />Save</Button>
                            <Button onClick={() => this.handleClose()} color='red' inverted><Icon name='remove' />Cancel</Button>
                        </Modal.Actions>
                    </Form>
                </Modal.Content>
            </Modal>
        )
    }
}