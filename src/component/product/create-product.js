import React, {Component} from 'react'
import { Container, Grid, Form, Button, Icon } from 'semantic-ui-react'
import { MessageFormSuccess} from '../../container/utils'

export class CreateNewProduct extends Component {
    constructor(props){
        super(props)
        this.state = {
            success: 0,
            message: 'You must fill the fields to before you save.',
            product_name: '',
            product_type: '',
            product_supplier: '',
            product_category: '',
            buying_price: '',
            product_manufacturer: '',
            shopList: []
        }
    }

    setSuccess =(success)=>{
        this.setState({
            success: success,
            message: ''
        })
    }

    handleSubmit = (e) => {
        let {product_name, product_type, product_supplier, product_category, buying_price, product_manufacturer} = this.state
        const data = {
            product_name,
            product_type, 
            product_supplier,
            product_category,
            product_manufacturer,
            buying_price
        }

        if(product_name ==="" || product_type==="" || product_supplier==="" || product_category==="" || product_manufacturer==="" || buying_price===""){
            return (
                this.setState({
                    success: 1,
                    message: 'You must fill the fields to before you save.'
                })
            )
        }

        fetch(`/api/productsAPI/createNew`, { method: 'POST', body: JSON.stringify(data)})
        .then(response => response.json())
        .then(res=> {
            console.log(res)
            let successCode = res.success;
            let message = res.message
            if(successCode === 1){
                this.setState({
                    success: 2,
                    message: message,
                    product_name: '',
                    product_type: '',
                    product_supplier: '',
                    product_category: '',
                    buying_price: '',
                    product_manufacturer: ''
                })
            }else {
                this.setState({
                    success: 1, 
                    message: message
                })
            }
        })
    }

    onChange = (e, value) => {
        this.setState({ 
            [e.target.name]: e.target.value, 
            success: 0
        })
    }
    
    render(){
        let {success, message} = this.state
        return (
            <Container>
                <Grid style={{marginTop: '10px'}} textAlign="center">
                    <MessageFormSuccess visible={success} setSuccess={this.setSuccess} message={message}/>
                </Grid>
                <Grid>
                    <Grid.Column width='4'></Grid.Column>
                    <Grid.Column width='8' textAlign='right'>
                        <Form onSubmit={(e) => this.handleSubmit(e)}>
                            <Form.Field>
                                <input type="text" name="product_name" value={this.state.product_name} placeholder="Product Name" onChange={(e) => this.onChange(e)} />
                            </Form.Field>
                            <Form.Field>
                                <input type="text" name="product_type" value={this.state.product_type} placeholder="Product Type" onChange={(e) => this.onChange(e)} />
                            </Form.Field>
                            <Form.Field>
                                <input type="text" name="product_category" value={this.state.product_category} placeholder="Product Category" onChange={(e) => this.onChange(e)} />
                            </Form.Field>
                            <Form.Field>
                                <input type="text" name="product_manufacturer" value={this.state.product_manufacturer} placeholder="Manufacturer" onChange={(e) => this.onChange(e)} />
                            </Form.Field>
                            <Form.Field>
                                <input type="text" name="product_supplier" value={this.state.product_supplier} placeholder="Product supplier" onChange={(e) => this.onChange(e)} />
                            </Form.Field>
                            <Form.Field>
                                <input type="number" name="buying_price" value={this.state.buying_price} placeholder="Buying Price" onChange={(e) => this.onChange(e)} />
                            </Form.Field>
                            <Form.Field>
                                <Button type="submit" color='green' inverted><Icon name='checkmark' />Save</Button>
                            </Form.Field>
                        </Form>
                    </Grid.Column>
                    <Grid.Column width='4'></Grid.Column>
                </Grid>
            </Container>
        )
    }
}