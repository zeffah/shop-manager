import React from 'react'
import { Link} from 'react-router-dom'
import {List, Button, Divider, Grid, Container} from 'semantic-ui-react'
import {ModalEditProduct} from './modalEditProduct'
import {ModalDeleteProduct} from './modalDeleteProduct'
import {ModalAddToInventory} from './modalAddToInventory'

export class ProductComponent extends React.Component {
    constructor(){
        super()
        this.state = {
            productList: [],
            product: {},
            showModal: false
        }
        this.editProduct = this.editProduct.bind()
    }

    editProduct = (product) => {
        this.setState({
            showModal: true,
            product: product
        })
    }

    deletetProduct = (product) => {
        this.setState({
            showModalDelete: true,
            product: product
        })
    }

    addProductToInventory = (product) => {
        this.setState({
            showModalAddInventory: true,
            product: product
        })
    }

    editModalOpened = (opened) => {
        this.setState({
            showModal: opened
        })
    }

    deleteModalOpened = (opened, isDelete) => {
        this.setState((prevState) => ({
            showModalDelete: opened,
            productList: isDelete ? prevState.productList.filter(item => item !== prevState.product) : prevState.productList
        }))
    }

    addToInventoryModalOpened = (opened, isDelete) => {
        this.setState((prevState) => ({
            showModalAddInventory: opened
        }))
    }

    componentDidMount() {
        fetch('/api/productsAPI/list')
        .then(response => response.json())
        .then(jsonResponse => {
            this.setState({
                productList: jsonResponse.products
            })
        })
        .catch(err => console.log('Error', err))
    }

    render() {
        let productList = this.state.productList
        return (
            <Container style={{ padding: '10px' }}>
                <Grid>
                    <Grid.Column width={8}><h3>Product List</h3></Grid.Column>
                    <Grid.Column textAlign='right' width={8}><Link as="button" to='product' icon="add"><Button color='green' inverted>Create Product</Button></Link></Grid.Column>
                </Grid>
                <Divider />
                <List divided relaxed>
                    {
                        productList.map((product, key) => (
                            <List.Item key={key}>
                                <List.Content floated='right'>
                                    <Button icon='currency' label={{ basic: true, content: product.buying_price }} labelPosition='right' />
                                    <Button onClick={() => this.addProductToInventory(product)} circular color="green" icon="plus" />
                                    <Button onClick={() => this.editProduct(product)} circular color="facebook" icon="edit" />
                                    <Button onClick={() => this.deletetProduct(product)} circular color="youtube" icon="delete" />
                                </List.Content>
                                <List.Icon name='list' size='large' verticalAlign='middle' />
                                <List.Content>
                                    <List.Header>{product.product_name} - {product.product_type} </List.Header>
                                    <List.Description>{product.product_category}</List.Description>
                                    <List.Description>{product.product_supplier}</List.Description>
                                </List.Content>
                            </List.Item>
                        ))
                    }
                </List> 
                <ModalEditProduct product={this.state.product} open={this.state.showModal} editModalOpen={this.editModalOpened} />
                <ModalDeleteProduct product={this.state.product} open={this.state.showModalDelete} deleteModalOpen={this.deleteModalOpened} />
                <ModalAddToInventory product={this.state.product} open={this.state.showModalAddInventory} addToInventoryModalOpen={this.addToInventoryModalOpened} />
            </Container>
        )
    }
}

// const ProductList = ({ listArray })=> (
//     listArray.map((product, key) => (
//         <List.Item key={key}>
//             <List.Content floated='right'>
//                 <Button icon='currency' label={{basic: true, content: product.buying_price }} labelPosition='right' />
//                 <Button onClick={() => console.log(`Add ${product.product_name} to inventory`)} circular color="green" icon="plus" />
//                 <Button onClick={() => this.editProduct(product)} circular color="facebook" icon="edit" />
//                 <Button onClick={() => console.log(`Delete ${product.product_name} from product list `)} circular color="youtube" icon="delete" />
//             </List.Content>
//             <List.Icon name='list' size='large' verticalAlign='middle' />
//             <List.Content>
//                 <List.Header>{product.product_name} - {product.product_type} </List.Header>
//                 <List.Description>{product.product_category}</List.Description>
//                 <List.Description>{product.product_supplier}</List.Description>
//             </List.Content>
//         </List.Item>
//     ))
// )