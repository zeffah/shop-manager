import React from 'react'
import { Link } from 'react-router-dom'
import { Dropdown, Menu } from 'semantic-ui-react'
import CryptoJs, {AES} from "crypto-js"

export const DropdownFloating = ({user, history}) => (
    <Dropdown text={getUsername(user)} pointing className='link item'>
        <Dropdown.Menu>
            <Link to="/" className="item"><i className="user icon"></i>Profile</Link>
            <Dropdown.Divider />
            <Menu.Item name="Logout"
                icon="sign out alternate"
                onClick={()=>handleItemClick(history)} />
        </Dropdown.Menu>
    </Dropdown>
)

const getUsername = (user)=>{
    let bytes = AES.decrypt(user.toString(), '[ZEFF-PETWANA-APP]');
    let decryptedData = JSON.parse(bytes.toString(CryptoJs.enc.Utf8));
    return decryptedData.email_address
}

const handleItemClick = (history) => {
    localStorage.clear()
    history.push('/login')
}