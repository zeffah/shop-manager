import React from 'react'
import { Button, Header, Icon, Modal, Form } from 'semantic-ui-react'
import { SHA256 } from "crypto-js";
export class AddShopKeeper extends React.Component {
    state = {
        shop_id: "",
        first_name: "",
        last_name: "",
        phone_number: "",
        national_id: "",
        username: "",
        password: "",
        email_address: "",
    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    handleClose = () => {
        this.setState({
            shop_id: "",
            first_name: "",
            last_name: "",
            phone_number: "",
            national_id: "",
            username: "",
            password: "",
            email_address: "",
        })
        this.props.addShopkeeperModalOpen(false)
    }

    handleSubmit = () => {
        let {first_name, last_name, phone_number, national_id, email_address} = this.state
        let passKey = last_name.toLowerCase()
        let data = {
            first_name, 
            last_name,
            phone_number,
            national_id, 
            email_address, 
            username: email_address,
            shop_id: this.props.shop.id,
            password: SHA256(`${passKey}@petwana`).toString()
        }
        console.log(data)

        fetch(`/api/usersAPI/register`, {method: 'POST', body: JSON.stringify(data)})
        .then(res=>res.json())
        .then(response=> {
            console.log(response)
            this.handleClose()
        })
        .catch(error=>console.log(error))
    }

    render() {
        let { shop } = this.props
        return (
            <Modal basic size='small' open={this.props.open} onClose={() => this.handleClose()}>
                <Header icon='plus' color="green" content={`Add Shopkeeper - ${shop.shop_name}`} />
                <Modal.Content>
                    <Form onSubmit={(e) => this.handleSubmit(e)}>
                        <Form.Field>
                            <input type="text" required placeholder="First Name" name="first_name" value={this.state.first_name} onChange={(e) => this.onChange(e)} />
                        </Form.Field>
                        <Form.Field>
                            <input type="text" required placeholder="Last Name" name="last_name" value={this.state.last_name} onChange={(e) => this.onChange(e)} />
                        </Form.Field>
                        <Form.Field>
                            <input type="text" required placeholder="Phone Number" name="phone_number" value={this.state.phone_number} onChange={(e) => this.onChange(e)} />
                        </Form.Field>
                        <Form.Field>
                            <input type="email" required placeholder="email_address" name="email_address" value={this.state.email_address} onChange={(e) => this.onChange(e)} />
                        </Form.Field>
                        <Form.Field>
                            <input type="number" required placeholder="National Id number" name="national_id" value={this.state.national_id} onChange={(e) => this.onChange(e)} />
                        </Form.Field>
                        <Modal.Actions>
                            <Button type="submit" color='green' inverted><Icon name='checkmark' />Save</Button>
                            <Button onClick={() => this.handleClose()} color='red' inverted><Icon name='remove' />Cancel</Button>
                        </Modal.Actions>
                    </Form>
                </Modal.Content>
            </Modal>
        )
    }
}