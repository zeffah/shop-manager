import React, {Component} from 'react'
import { Button, Form, Grid, Header, Container } from 'semantic-ui-react'
import { SHA256 } from "crypto-js"
import { MessageFormSuccess } from '../../container/utils'

export class RegisterComponent extends Component {
    state = {
        first_name: '',
        last_name: '',
        phone_number: '',
        email_address: '',
        username: '',
        password: '',
        verified: 0,
        success: 0,
        message: 'All Fields are required.',
    }
    setSuccess = (success) => {
        this.setState({
            success: success,
            message: ''
        })
    }

    onChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    navigate = () => {
        const { history } = this.props
        history.push('/login')
    }

    formSubmit=()=>{
        const { history } = this.props
        let {first_name, last_name, email_address, phone_number, username, password, verified} = this.state
        const data = {
            first_name, last_name, email_address, phone_number, username, password: SHA256(password).toString(), verified
        }
        console.log(data)
        fetch(`/api/usersAPI/register-admin`, {method: 'POST', body: JSON.stringify(data)})
        .then(res=>res.json())
        .then(response=>{
            console.log(response)
            if(response.success === 1){
                this.setState({
                    first_name: '',
                    last_name: '',
                    phone_number: '',
                    email_address: '',
                    username: '',
                    password: '',
                })
                history.push('/login')
            } else {
                this.setState({
                    success: 1,
                    message: response.message,
                })
            }
        })
        .catch(error=>console.log(error))
    }
    render(){
        let { success, message } = this.state
        return(
            <div className="login-form" >
                <style>{`
                body > div,
                body > div > div,
                body > div > div > div.login-form {
                    height: 100%;
                }
                `}</style>
                <Grid textAlign='center' verticalAlign='middle' style={{ height: '100%' }}>
                    <Grid.Column style={{ maxWidth: 500 }}>
                        <MessageFormSuccess visible={success} setSuccess={this.setSuccess} message={message} />
                        <Header as='h2' color='teal' textAlign='center'>
                            Register
                        </Header>
                        <Form size='large' onSubmit={(e) => this.formSubmit()}>
                            <Form.Group widths='equal'>
                                <Form.Input required fluid icon='user' iconPosition='left' name="first_name" placeholder='First name' onChange={(e) => this.onChange(e)} />
                                <Form.Input required fluid icon='user' iconPosition='left' name="last_name" placeholder='Last name' onChange={(e) => this.onChange(e)}/>
                            </Form.Group>
                            <Form.Group widths='equal'>
                                <Form.Input required fluid icon='phone' iconPosition='left' name="phone_number" placeholder='Phone Number' onChange={(e) => this.onChange(e)} />
                                <Form.Input required fluid icon='mail' iconPosition='left' name="email_address" placeholder='Email Address' onChange={(e) => this.onChange(e)} />
                            </Form.Group>
                            <Form.Input required fluid icon='user' iconPosition='left' name='username' placeholder='Username' type='text' onChange={(e) => this.onChange(e)} />
                            <Form.Input required fluid icon='lock' iconPosition='left' name='password' placeholder='Password' type='password' onChange={(e) => this.onChange(e)} />
                            <Button type="submit" color='green' inverted fluid size='large'>Sign Up</Button>
                        </Form>
                        <Button onClick={() => this.navigate()} style={{ marginTop: 10 }} content="Already Registerd? Sign In" color='black' fluid size='large' />
                    </Grid.Column>
                </Grid>
            </div>
        )
    }
}