import React, { Component } from "react"
import { Grid, Table, TableBody, Button } from "semantic-ui-react";

export class Shopkeepers extends Component {
    state = {
        shopkeepers: []
    }
    componentDidMount() {
        fetch(`/api/usersAPI/shopkeepers`)
        .then(res=>res.json())
        .then(response=>{
            let success = response.success
            if(success === 1){
                this.setState({
                    shopkeepers: response.shopkeepers
                })
            }
        })
        .catch(error=>console.log(error))
    }

    render(){
        let {shopkeepers} = this.state
        return (
            <div>
            <Grid columns={1}>
                <Grid.Column width={16}>
                    <Table>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Staff Name</Table.HeaderCell>
                                <Table.HeaderCell>Phone Number</Table.HeaderCell>
                                <Table.HeaderCell>Email Address</Table.HeaderCell>
                                <Table.HeaderCell>Shop Name</Table.HeaderCell>
                                <Table.HeaderCell>Action</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <TableBody>
                            {
                                shopkeepers.map((shopkeeper, i)=>
                                    <Table.Row key={i}>
                                        <Table.Cell>{shopkeeper.staff_name}</Table.Cell>
                                        <Table.Cell>{shopkeeper.phone_number}</Table.Cell>
                                        <Table.Cell>{shopkeeper.email_address}</Table.Cell>
                                        <Table.Cell>{shopkeeper.shop_name}</Table.Cell>
                                        <Table.Cell>
                                            <Button.Group as="div" size='small'>
                                                <Button color="green" icon='edit'/>
                                                <Button color="red" icon='delete' />
                                            </Button.Group>
                                        </Table.Cell>
                                    </Table.Row>
                                )
                            }
                        </TableBody>
                    </Table>
                </Grid.Column>
            </Grid>
            </div>
        )
    }
}