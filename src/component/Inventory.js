import React,{Component} from 'react';
import { Button,Table, Divider } from 'semantic-ui-react';
import {getShopInventoryService} from "../services/inventoryService";

class Inventory extends Component {
    
    render() {
        var inventorylist= getShopInventoryService();
        return(
            <Table striped>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Product Name</Table.HeaderCell>
                <Table.HeaderCell>Type</Table.HeaderCell>
                <Table.HeaderCell>Cost</Table.HeaderCell>
                <Table.HeaderCell>Selling Price</Table.HeaderCell>
                <Table.HeaderCell>Quantity</Table.HeaderCell>
                <Table.HeaderCell>Inventory value</Table.HeaderCell>
                <Table.HeaderCell>Action</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
    
            <Table.Body>
               {
                 inventorylist.map((product, i) => {
                   return   <Table.Row key={i}>
                                <Table.Cell>{product.Name}</Table.Cell>
                                <Table.Cell>{product.Type+"kgs"}</Table.Cell>
                                <Table.Cell>{product.Cost}</Table.Cell>
                                <Table.Cell>{product.Selling}</Table.Cell>
                                <Table.Cell>{product.Quantity}</Table.Cell>
                                <Table.Cell>{product.Quantity * product.Selling}</Table.Cell>
                                <Table.Cell>
                                    <Button.Group as="div" size='small'>
                                        <Button color="green" icon='edit' />
                                        <Button color="red" icon='delete' />
                                    </Button.Group>
                                </Table.Cell>
                            </Table.Row>
                 })
                }
              
            </Table.Body>
          </Table>
        )
    }
}

export default Inventory;