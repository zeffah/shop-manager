import React from 'react'
import { Button, Header, Icon, Modal, Form } from 'semantic-ui-react'

export class ModalUpdateInventory extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            selling_price: this.props.inventory.selling_price,
            quantity: this.props.inventory.quantity,
            id: this.props.inventory.inventory_id,
        }
    }

    handleClose = (e) => {
        e.preventDefault()
        this.props.editModalOpen(false)
    }

    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    componentWillReceiveProps(nextProps){
        this.setState({
            selling_price: nextProps.inventory.selling_price,
            quantity: nextProps.inventory.quantity,
            id: nextProps.inventory.inventory_id
        })
    }
    
    handleSubmit = (e) => {
        e.preventDefault()
        fetch('/api/inventoryAPI/update', { method: 'POST', body: JSON.stringify(this.state)})
        .then(success=>{
            return success.json()
        }).then(response=>{
            let data = response.data
            this.props.inventory.selling_price = data.selling_price
            this.props.inventory.quantity = data.quantity
            this.props.editModalOpen(false)
        }).catch((err)=>{
            console.log(err)
        })
    }
    
    render(){
        let { inventory } = this.props
        return (
            <Modal basic size='small' open={this.props.open} onClose={() => this.props.editModalOpen(false)}>
                <Header icon='edit' content={`Update Inventory - ${inventory.product_name}`} />
                <Modal.Content>
                    <Form onSubmit={(e) => this.handleSubmit(e)}>
                        <Form.Field>
                            <h3>{`New Selling Price`}</h3>
                            <input type="number" name="selling_price" value={this.state.selling_price} onChange={(e) => this.onChange(e)} />
                        </Form.Field>
                        <Form.Field>
                            <h3>{`New Quantity`}</h3>
                            <input type="number" name="quantity" value={this.state.quantity} onChange={(e) => this.onChange(e)} />
                        </Form.Field>
                        <Modal.Actions>
                            <Button type="submit" color='green' inverted><Icon name='checkmark' />Save</Button>
                            <Button onClick={(e) => this.handleClose(e)} color='red' inverted><Icon name='remove' />Cancel</Button>
                        </Modal.Actions>
                    </Form>
                </Modal.Content>
            </Modal>
        )
    }
}