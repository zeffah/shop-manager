import React, {Component} from 'react'
import { Button, Header, Icon, Modal, Form} from 'semantic-ui-react'

export class ModalDeleteInventory extends Component {
    handleClose = (e, isDelete) => {
        e.preventDefault()
        this.props.deleteModalOpen(false, isDelete)
    }

    handleSubmit = (e) => {
        e.preventDefault()
        let inventory = this.props.inventory
        fetch(`/api/inventoryAPI/delete`, { method: 'POST', body: JSON.stringify({ id: inventory.inventory_id })})
            .then(success => {
                return success.json()
            }).then(response => {
                this.props.deleteModalOpen(false, true)
            }).catch((err) => {
                console.log(err)
            })
    }

    render(){
        let { inventory } = this.props
        return (
            <Modal basic size='small' open={this.props.open} onClose={() => this.props.deleteModalOpen(false, false)}>
                <Header icon='remove' color='red' content={`Delete Inventory - ${inventory.product_name}`} />
                <Modal.Content>
                <h3>{`Are you sure you want to delete ${inventory.product_name} - ${inventory.product_type}?`}</h3>
                    <Form onSubmit={(e) => this.handleSubmit(e)}>
                        <Modal.Actions>
                            <Button type="submit" color='green' inverted><Icon name='checkmark' />Yes, Delete</Button>
                            <Button onClick={(e) => this.handleClose(e, false)} color='red' inverted><Icon name='remove' />Cancel</Button>
                        </Modal.Actions>
                    </Form>
                </Modal.Content>
            </Modal>
        )
    }
}