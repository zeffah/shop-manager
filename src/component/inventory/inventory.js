import React from 'react'
import {Grid, List, Table, Button, Header, Icon, Divider} from 'semantic-ui-react'
import { ModalUpdateInventory } from './modalEditInventory'
import {ModalDeleteInventory} from './modalDeleteInventory'

export class InventoryComponent extends React.Component {
    constructor(){
        super()
        this.state = {
            shops: [],
            inventoryList: [],
            shopName: [],
            showModal: false,
            inventory: {}
        }
        this.editInventory = this.editInventory.bind()
    }

    onItemClick(shop) {
        this.setState({
            shopName: shop.shop_name,
            inventoryList: shop.inventory
        })
    }

    editModalOpened = (opened) => {
        this.setState({
            showModal: opened
        })
    }

    deleteModalOpened = (opened, isDelete) => {
        this.setState((prevState)=>({
            showModalDelete: opened,
            inventoryList: isDelete?prevState.inventoryList.filter(item => item !== prevState.inventory):prevState.inventoryList
        }))
    }

    editInventory = (inventory) => {
        this.setState({
            showModal: true,
            inventory: inventory
        })
    }

    deletetInventory = (inventory) => {
        this.setState({
            showModalDelete: true,
            inventory: inventory
        })
    }

    componentDidMount(){
        fetch(`/api/shopsAPI/shops`)
        .then(response => response.json())
        .then(res => {
            this.setState({
                shops: res.shops, 
                inventoryList: res.shops[0].inventory,
                shopName: res.shops[0].shop_name})
        })
        .catch(error=>console.log(error))
    }
    render(){
        let shopsList = this.state.shops
        let shopInventory = this.state.inventoryList
        return (
            <Grid>
                <Grid.Column width={5}>
                    <List divided relaxed>
                        {shopsList.map((shop, key) => (
                            <List.Item key={key}>
                                <List.Icon name='home' size='large' verticalAlign='middle' />
                                <List.Content onClick={() => this.onItemClick(shop)}>
                                    <List.Header as='a'>{shop.shop_name}{shop.email_address}</List.Header>
                                    <List.Description as='a'>{shop.town}</List.Description>
                                </List.Content>
                            </List.Item>
                        ))}
                    </List>
                </Grid.Column>
                <Grid.Column width={11}>
                    <Header as='h3'>
                        <Icon name='warehouse' />
                        <Header.Content>{`${this.state.shopName}`}</Header.Content>
                    </Header>
                    <Divider/>
                    <Table striped>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Product Name</Table.HeaderCell>
                                <Table.HeaderCell>Type</Table.HeaderCell>
                                <Table.HeaderCell>Cost</Table.HeaderCell>
                                <Table.HeaderCell>Selling Price</Table.HeaderCell>
                                <Table.HeaderCell>Quantity</Table.HeaderCell>
                                <Table.HeaderCell>Inventory value</Table.HeaderCell>
                                <Table.HeaderCell>Action</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>

                        <Table.Body>
                            {
                                shopInventory.map((product, i) => {
                                    return <Table.Row key={i}>
                                        <Table.Cell>{product.product_name}</Table.Cell>
                                        <Table.Cell>{product.product_type}</Table.Cell>
                                        <Table.Cell>{product.buying_price}</Table.Cell>
                                        <Table.Cell>{product.selling_price}</Table.Cell>
                                        <Table.Cell>{product.quantity}</Table.Cell>
                                        <Table.Cell>{product.quantity * product.selling_price}</Table.Cell>
                                        <Table.Cell>
                                            <Button.Group as="div" size='small'>
                                                <Button color="green" icon='edit' onClick={() => this.editInventory(product)} />
                                                <Button color="red" icon='delete' onClick={() => this.deletetInventory(product)}/>
                                            </Button.Group>
                                        </Table.Cell>
                                    </Table.Row>
                                })
                            }

                        </Table.Body>
                    </Table>
                </Grid.Column>
                <ModalUpdateInventory inventory={this.state.inventory} open={this.state.showModal} editModalOpen={this.editModalOpened} />
                <ModalDeleteInventory inventory={this.state.inventory} open={this.state.showModalDelete} deleteModalOpen={this.deleteModalOpened} />
            </Grid>
        )
    }
}