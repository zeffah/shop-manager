import React from 'react'
import { Card, Button, Image, Segment, Grid, Label, Divider } from 'semantic-ui-react'

export class ShopComponent extends React.Component {
    render() {
        let sales = [{ shopName: "Shop 1", keeper: 'Elijah', inventoryValue: "Ksh.100,000", totalSales: '200,000' },
        { shopName: "Shop 2", keeper: 'Zadiki', inventoryValue: "Ksh.500,000", totalSales: '700,000' }]
        return (
            <Segment>
                <Grid>
                    <Grid.Row columns={4}>
                    {sales.map((item, position) => {
                        console.log(item)
                            return <ShopItem key={position} item={item} />
                    })}
                    </Grid.Row>
                </Grid>
            </Segment>
        )
    }
}

const ShopItem = ({item}) => (
    <Grid.Column>
        <Card>
            <Card.Content>
                <Image floated='right' size='mini' src='https://react.semantic-ui.com/images/avatar/large/steve.jpg' />
                <Card.Header>{item.shopName}</Card.Header>
                <Card.Meta>{item.keeper}</Card.Meta>
                <Card.Description>
                    {/* <Menu compact>
                        <Menu.Item as='a'>
                            <Icon name='mail' /> Inventory
                            <Label color='red' floating>
                                {item.inventoryValue}
                            </Label>
                        </Menu.Item>
                        <Menu.Item as='a'>
                            <Icon name='users' /> Sales
                            <Label color='teal' floating>
                                {item.totalSales}
                            </Label>
                        </Menu.Item>
                    </Menu> */}
                    {/* <Label>
                        Inventory<Label.Detail>{item.inventoryValue}</Label.Detail>
                    </Label>
                    <Label>
                        Sales<Label.Detail>{item.totalSales}</Label.Detail>
                    </Label> */}
                    {/* Inventory Value:&nbsp;<strong>{item.inventoryValue}</strong> */}
                </Card.Description>
            </Card.Content>
            <Card.Content extra>
                <Grid>
                    <Grid.Row columns={3}>
                        <Grid.Column>
                            <Grid.Row>
                                <Label.Detail as='a'>Inventory</Label.Detail>
                            </Grid.Row>
                            <Grid.Row>
                                <Label>{item.inventoryValue}</Label>
                            </Grid.Row>
                        </Grid.Column>
                        <Grid.Column>
                            <Divider vertical></Divider>
                        </Grid.Column>
                        <Grid.Column>
                            <Grid.Row>
                                <Label as='a'>Total Sales</Label>
                            </Grid.Row>
                            <Grid.Row>
                                <Label.Detail>{item.totalSales}</Label.Detail>
                            </Grid.Row>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Card.Content>
            <Card.Content extra>
                <div className='ui two buttons'>
                    <Button basic color='green'>
                        Approve
                    </Button>
                    <Button basic color='red'>
                        Decline
                    </Button>
                </div>
            </Card.Content>
        </Card>
    </Grid.Column>
)