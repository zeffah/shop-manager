import React from 'react'
import { Redirect } from 'react-router-dom'
import { Grid } from 'semantic-ui-react'
import CryptoJs, { AES } from "crypto-js"
import { MainRouter} from '../container/router'
import {MainNav} from '../container/nav'
import {SideBarComponent} from '../container/sidebar'

class Main extends React.Component {
    render() {
        let user = localStorage.getItem('user')

        const {history} = this.props
        if(getUsername(user)){
            return (
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={3}>
                            <SideBarComponent history={history}/>
                        </Grid.Column>
                        <Grid.Column width={12} style={{marginLeft: '5px'}}>
                            <MainNav history={history} user={user}/>
                            <MainRouter/>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            )
        }
        return (
            <Redirect to='/login'/>
        )
    }
}

const getUsername = (user) => {
    let bytes = AES.decrypt(user.toString(), '[ZEFF-PETWANA-APP]');
    let decryptedData = JSON.parse(bytes.toString(CryptoJs.enc.Utf8));
    return decryptedData.email_address
}

export default Main