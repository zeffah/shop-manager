import React from 'react'
import { Message } from 'semantic-ui-react'
export class MessageFormSuccess extends React.Component {
    state = { visible: this.props.visible }
    handleDismiss = () => {
        this.setState({ 
            visible: this.props.setSuccess(0), 
            message: this.props.message})
    }

    componentWillReceiveProps(nextProps){
        this.setState({
            visible: nextProps.visible,
            message: nextProps.message
        })
    }

    render() {
        let {message} = this.state
    if (this.state.visible === 1) {
      return (
          <Message negative onDismiss={this.handleDismiss}>
              <Message.Header>Error</Message.Header>
              <p>{message}</p>
          </Message>
      )
    }
    if(this.state.visible === 2){
        return (
            <Message positive onDismiss={this.handleDismiss}>
                <Message.Header>Product Creation success</Message.Header>
                <p>{`Congratulation!,${message}. You can now add it to inventory from product list page.`}</p>
            </Message>
        )
    }
    return(
        <div></div>
    )
  }
}

export const TotalSalesAmount = (sales) => {
    let sum = 0;
    sales.map(sale => (
        sum += sale.sale_amount * 1
    ))
    return sum
}

export const ShopsSales = (shopsList) => {
    let sales = []
    shopsList.map(shop => (
        shop.salesList.map(_sl => (sales.push(_sl)))
    ))
    return sales
}

export const InventoryList = (shopsList) => {
    let _inventoryList = []
    shopsList.map(shop => (
        shop.inventory.map(i => (_inventoryList.push(i)))
    ))
    return _inventoryList
}

export const TotalInventoryValue = (inventoryList) => {
    let sum = 0
    inventoryList.map(inventory => (
        sum += inventory.selling_price * inventory.quantity
    ))
    return sum
}

export const InventoryPieChartDataset = (shopsList) => {
    let _inventoryList = []
    shopsList.map(shop => (
        _inventoryList.push(TotalInventoryValue(shop.inventory))
    ))
    return _inventoryList
}

export const SalesPieChartDataset = (shopsList)=>{
    let _saleList = []
    shopsList.map(shop=>(
        _saleList.push(TotalSalesAmount(shop.salesList))
    ))
    return _saleList
}