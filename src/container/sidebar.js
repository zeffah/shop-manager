import React from 'react'
import { Link } from 'react-router-dom'
import { Menu, Sidebar, Accordion, Icon } from 'semantic-ui-react'

export class SideBarComponent extends React.Component {
    state = { activeIndex: -1 }

    handleClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeIndex } = this.state
        const newIndex = activeIndex === index ? -1 : index

        this.setState({ activeIndex: newIndex })
    }
    render(){
        const { activeIndex } = this.state
        const {history} = this.props
        return (
            <Sidebar as={Menu} direction='left' inverted vertical visible style={{ width: '19%' }}>
                <Menu.Item as={Link} header to='/'><Icon name='home' size='large' />Home</Menu.Item>
                <Menu.Item as={Link} to='products'><Icon name='ordered list' size='large' />Products</Menu.Item>
                <Menu.Item as={Link} to='inventory'><Icon name='warehouse' size='large'/>Inventory</Menu.Item>
                <Menu.Item as={Link} to='sales'><Icon name='unordered list' size='large'/>Sales</Menu.Item>
                <Menu.Item as={Link} to="sale-stats"><Icon name='chart line' size='large'/>Sales Statistics</Menu.Item>
                <Menu.Item as={Link} to="shopkeepers"><Icon name='user' size='large'/>Our Shopkeeers</Menu.Item>
                <Menu.Item onClick={()=>handleItemClick(history)}><Icon name='sign out alternate' size='large'/>Logout</Menu.Item>
                {/* <Menu.Item><Icon name='home' size='large'/>
                    <Accordion inverted>
                        <Accordion.Title active={activeIndex === 0} index={0} onClick={this.handleClick}>
                            <Icon name='dropdown' />
                            Sales
                        </Accordion.Title>
                        <Accordion.Content active={activeIndex === 0}>
                        <Menu inverted vertical>
                            <Menu.Item as={Link} to='sales'>Sales List</Menu.Item>
                        </Menu>
                        <Menu inverted vertical>
                            <Menu.Item as={Link} to='sale-stats'>Sales Statistics</Menu.Item>
                        </Menu>
                        </Accordion.Content>
                    </Accordion>
                </Menu.Item> */}
            </Sidebar>
        )
    }
}

const handleItemClick = (history) => {
    localStorage.clear()
    history.push('/login')
}