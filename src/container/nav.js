import React from 'react'
import {Link} from 'react-router-dom'
import {DropdownFloating} from '../component/dropdown'
import { Menu } from 'semantic-ui-react';

export const MainNav = ({history, user}) => (
    <Menu secondary pointing>
        <Link to="/" className="item"><i className="home icon"></i>Home</Link>
        <Link to="sales" className="item"><i className="unordered list icon"></i>Sales</Link>
        <Link to="products" className="item"><i className="ordered list icon"></i>Products</Link>
        <Menu.Menu position='right'>
            <DropdownFloating user={user} history={history} />
        </Menu.Menu>
    </Menu>
)