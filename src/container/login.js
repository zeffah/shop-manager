import React, {Component} from 'react'
import { Link, Redirect } from 'react-router-dom'
import { AES, SHA256} from "crypto-js";
import { Button, Form, Grid, Header, Container } from 'semantic-ui-react'
import { MessageFormSuccess } from './utils'
class Login extends Component {
    state = {
        username: '',
        password: '',
        success: 0,
        message: 'All Fields are required.',
    }

    setSuccess = (success) => {
        this.setState({
            success: success,
            message: ''
        })
    }

    onChange = (e)=> {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    navigate = () => {
        const { history } = this.props
        history.push('/signup')
    }

    formSubmit = () => {
        const { history } = this.props
        let {username, password} = this.state
        if (username === "" || password === "") {
            return (
                this.setState({
                    success: 1,
                    message: 'All fields must be filled'
                })
            )
        }
        fetch('/api/usersAPI/adminLogin', { method: 'POST', body: JSON.stringify({ username: username, password: SHA256(password).toString() })})
        .then(res=>res.json())
        .then(response=>{
            let _success = response.success
            let message = response.message
            let user = JSON.stringify(response.user)
            if(_success ===1){
                this.setState({
                    username: '',
                    password: ''
                })
                localStorage.setItem('user', AES.encrypt(user, '[ZEFF-PETWANA-APP]'))
                history.push("/")
            }else {
                this.setState({
                    success: 1,
                    message: message,
                })
            }
        })
        .catch(error=> console.log(error))
    }
    render() {
        let { success, message } = this.state
        let {history} = this.props
        if (localStorage.getItem('user')) {
            return <Redirect to = '/' />
        }
        return (
            <div className="login-form" >
                <style>{`
                body > div,
                body > div > div,
                body > div > div > div.login-form {
                    height: 100%;
                }
                `}</style>
                <Grid textAlign='center' verticalAlign='middle' style={{ height: '100%' }}>
                    <Grid.Column style={{ maxWidth: 350 }}>
                        <MessageFormSuccess visible={success} setSuccess={this.setSuccess} message={message} />
                        <Header as='h2' color='teal' textAlign='center'>
                            Log In
                        </Header>
                        <Form size='large' onSubmit={(e)=>this.formSubmit()}>
                            <Form.Input fluid icon='user' iconPosition='left' name='username' placeholder='Username' type='text' onChange={(e)=> this.onChange(e)} />
                            <Form.Input fluid icon='lock' iconPosition='left' name='password' placeholder='Password' type='password' onChange={(e)=>this.onChange(e)}/>
                            <Button type="submit" color='green' inverted fluid size='large'>Login</Button>
                        </Form>
                        <Container fluid style={{ marginTop: 5 }}>
                            <Link to="" color='white' onClick={() => console.log('onclick')}>Forgot Password?</Link>
                        </Container>
                        <Button onClick={()=>this.navigate()} style={{ marginTop: 10 }} content="No Account Yet? Register Here" color='black' fluid size='large' />
                    </Grid.Column>
                </Grid>
            </div>
        );
    }
}

export default Login