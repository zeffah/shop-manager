import React from 'react'
import { ProtectedRoute } from "../component/auth/protecte-route";
import {Dashboard} from './dashboard'
import {Profile} from '../component/profile'
import {ProductComponent} from '../component/product/product'
import { SaleComponent } from '../component/sales/sales'
import { Shopkeepers } from "../component/user/shopkeeper-list";
import { InventoryComponent } from '../component/inventory/inventory'
import { CreateNewProduct } from '../component/product/create-product'
import { SaleStatistics } from '../component/sales/stats'

let isAllowed = localStorage.getItem('user') !== null
export const MainRouter = () => (
    <div>
        <ProtectedRoute isAllowed={isAllowed} path="/" exact component={Dashboard} />
        <ProtectedRoute isAllowed={isAllowed} path="/profile" component={Profile} />
        <ProtectedRoute isAllowed={isAllowed} path="/products" component={ProductComponent} />
        <ProtectedRoute isAllowed={isAllowed} path="/shopkeepers" component={Shopkeepers} />
        <ProtectedRoute isAllowed={isAllowed} path="/sales" component={SaleComponent} />
        <ProtectedRoute isAllowed={isAllowed} path="/inventory" component={InventoryComponent} />
        <ProtectedRoute isAllowed={isAllowed} path="/product" component={CreateNewProduct} />
        <ProtectedRoute isAllowed={isAllowed} path="/sale-stats" component={SaleStatistics} />
    </div>
)