import React from 'react'
import {Grid, Card, Button, Statistic } from 'semantic-ui-react'
import NumberFormat from 'react-number-format'
import { AddShopKeeper } from "../component/user/modalAddShopkeeper";
import { TotalSalesAmount, TotalInventoryValue } from './utils'

export class Dashboard extends React.Component {
    state = {
        shopsList: [],
        shop: {}
    }
    addShopKeeper = (shop) => {
        this.setState({
            showModalAddShopekeeper: true,
            shop: shop
        })
    }

    addShopkeeperModalOpened = (opened) => {
        this.setState((prevState) => ({
            showModalAddShopekeeper: opened
        }))
    }

    componentDidMount() {
        fetch(`/api/shopsAPI/shops`).then(res => res.json()).then(response => {
            console.log(response)
            this.setState({
                shopsList: response.shops
            })
        }).catch(error=>console.log(error))
    }
    render(){
        const {shopsList} = this.state
        return (
            <Grid>
                <Grid.Row centered columns={3}>
                    {
                        shopsList.map(shop=>(
                            <Grid.Column key={shop.id}>
                                <Card>
                                    <Card.Content>
                                        <Card.Header>{shop.shop_name}</Card.Header>
                                        <Card.Meta>{shop.location}</Card.Meta>
                                        <Card.Description>
                                            {/* Steve wants to add you to the group <strong>best friends</strong> */}
                                            {shop.town}
                                        </Card.Description>
                                    </Card.Content>
                                    <Card.Content extra>
                                        <Statistic.Group size='mini' widths='two'>
                                            <Statistic color='green'>
                                                <Statistic.Value><NumberFormat value={TotalInventoryValue(shop.inventory)} displayType={'text'} thousandSeparator={true} prefix={''} /></Statistic.Value>
                                                <Statistic.Label>Inventory</Statistic.Label>
                                            </Statistic>
                                            <Statistic color='green'>
                                                <Statistic.Value><NumberFormat value={TotalSalesAmount(shop.salesList)} displayType={'text'} thousandSeparator={true} prefix={''} /></Statistic.Value>
                                                <Statistic.Label>Sales</Statistic.Label>
                                            </Statistic>
                                        </Statistic.Group>
                                    </Card.Content>
                                    <Card.Content extra textAlign='right'>
                                        <Button content='Add Shopkeeper' color='black' onClick={() => this.addShopKeeper(shop)}/>
                                    </Card.Content>
                                </Card>
                            </Grid.Column>
                        ))
                    }
                </Grid.Row>
                <AddShopKeeper shop={this.state.shop} open={this.state.showModalAddShopekeeper} addShopkeeperModalOpen={this.addShopkeeperModalOpened} />
                <Grid.Row></Grid.Row>
            </Grid>
        )
    }
}