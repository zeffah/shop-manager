import React from "react"
import { Grid } from "semantic-ui-react";
export const PageNotFound = () => (
    
    <Grid columns={1} centered>
        <Grid.Column>
            Page not found
        </Grid.Column>
    </Grid>
)