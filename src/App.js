import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom'
import Main from './container/main'
import Login from './container/login'
import { RegisterComponent } from "./component/user/register";
import { PageNotFound } from "./container/not-found";
import { ProtectedRoute } from "./component/auth/protecte-route"

class App extends Component {
  isAllowed = false
  user = localStorage.getItem('user')
  render() {
    if(this.user === null){
      this.isAllowed = false
    }else{
      this.isAllowed = true
    }

    return (
      <Switch>
        <Route component={Login} path="/login"/>
        <Route component={RegisterComponent} path="/signup" />
        <ProtectedRoute isAllowed={this.isAllowed} component={Main} path="/"/>
        <Route path="*" component={PageNotFound} />
      </Switch>
    )
  }
}

export default App
